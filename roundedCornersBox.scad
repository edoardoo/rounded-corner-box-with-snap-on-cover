box_corner_radius = 4;
box_width = 20;
box_height = 20;
box_z = 10;
box_thickness = 1.5;
cover_borderThickness = 1;
cover_thickness = 1;
cover_borderHeight = 2;
latch_ballsSize = 1;

printTolerance=0.01;

module half_cylinders( 
    inner=false, 
    height, 
    width, 
    thickness, 
    radius,
    z
    ){
    borderThickness = inner ? 0 : thickness/2;
    radius = inner? radius : radius+borderThickness ;
    for (i=[0:1]) {
        translate([
            (-1+(2*(i%2)))* ((width/2)-radius + borderThickness),
            ((height/2)-radius+borderThickness),
            0
        ])
        cylinder(r=radius, h=z, center=true);
    }
}

module innerBox(height,width, thickness, radius, z){
    hull(){
        half_cylinders(true, height, width, thickness, radius, z);
        mirror([0,1,0])
        half_cylinders(true, height, width, thickness, radius, z);
    }
}

module outerBox(height,width, thickness, radius, z){
    hull(){
        half_cylinders(false, height, width, thickness, radius, z);
        mirror([0,1,0])
        half_cylinders(false, height, width, thickness, radius, z);
    }
}

module latchBalls(inner=false, width, height, size){
    innerToleranceFix = inner? printTolerance: 0;
    for (i=[0:1]) {
        translate([
            (-1+(2*(i%2)))* (width/2),
            0,
            0
        ])
        sphere(d=size-innerToleranceFix);
    }
    for (i=[0:1]) {
        translate([
            0,
            (-1+(2*(i%2)))* (height/2),
            0
        ])
        sphere(d=size-innerToleranceFix);
    }
}

module roundedCornerBox(
    radius = box_corner_radius,
    width= box_width ,
    height = box_height,
    z = box_z,
    thickness = box_thickness,
    latchSize = latch_ballsSize,
    coverBorderHeight = cover_borderHeight
){
    difference(){

        outerBox( height=height, width=width, thickness=thickness, radius=radius, z=z );
        translate([0,0, box_thickness])
        innerBox( height=height, width=width, thickness=thickness, radius=radius, z=z );
        
        translate([
            0,
            0,
            (z/2) - latchSize/2 - ((coverBorderHeight - latchSize)/2)
        ])
        latchBalls(inner=false, width=width, height=height, size=latchSize );
    }
}

module roundedCornerBoxCover( 
    borderThickness=cover_borderThickness,
    borderHeight=cover_borderHeight, 
    width= box_width ,
    height = box_height,
    z=cover_thickness,
    thickness=box_thickness, 
    radius=box_corner_radius,
    latchSize=latch_ballsSize
    ){
    translate([ 
        0,
        0,
        -(thickness/2 + borderHeight/2)
    ])
    outerBox( height=height, width=width, thickness=borderThickness, radius=radius, z=thickness );
    translate([0,0,z/2-borderHeight/2]){
        difference(){
            scaleFactor = (width-2*borderThickness)/width;
            innerBox( height=height-printTolerance, width=width-printTolerance, thickness=thickness, radius=radius, z=borderHeight );
            scale([scaleFactor, scaleFactor, 1.01])
            innerBox( height=height, width=width, thickness=thickness, radius=radius, z=borderHeight );
        }   
        latchBalls(inner=true, width=width, height=height, size=latchSize );
    }
}