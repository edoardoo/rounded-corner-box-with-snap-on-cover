# Rounded Corners Box with Snap Cover

![alt](./img/preview.png)

**Attention**: dimensions are for the inner volume!!  
Usage: 

```
use <rounderCornersBox.scad>;
$fn=200;
roundedCornerBox(
    radius = 5,
    width= 20 ,
    height = 20,
    z = 20,
    thickness = 2,
    latchSize = 1,
    coverBorderHeight = 3
);
translate([40,0,0])
rounderCornerBoxCover(
    radius = 5,
    width= 20 ,
    height = 20,
    z=2,
    thickness = 2,
    latchSize = 1
);
```